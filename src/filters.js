import Vue from 'vue'

import { DateTime } from 'luxon'
import formatter from '@/services/formatters'

function formatCurrency(value) {
  return formatter.formatCurrency(value)
}

function formatNumber(value) {
  return formatter.formatNumber(value)
}

function formatDate(date) {
  const dt = DateTime.fromISO(date)
  return dt.toLocaleString()
}

function registerFilters() {
  Vue.filter('formatCurrency', formatCurrency)
  Vue.filter('formatDate', formatDate)
  Vue.filter('formatNumber', formatNumber)
}

export {
  registerFilters as default,
  formatCurrency,
  formatDate,
  formatNumber
}
