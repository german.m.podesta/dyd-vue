import Vue from 'vue'
import fontawesome from '@fortawesome/fontawesome'
import solid from '@fortawesome/fontawesome-free-solid'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

fontawesome.library.add(solid) // Use any icon from the Solid style

Vue.component('font-awesome-icon', FontAwesomeIcon)
