// bootstrap.js
const bluebird = require('bluebird')

bluebird.config({
  longStackTraces: true,
  warnings: true // note, run node with --trace-warnings to see full stack traces for warnings
})

require('babel-runtime/core-js/promise').default = bluebird
window.Promise = bluebird

// ...

require('./app')
