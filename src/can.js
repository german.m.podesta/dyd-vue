import auth from "@/services/auth";

const SimpleCanPlugin = {
  install(Vue, options) {
    Vue.prototype.$can = function(...args) {
      return auth.can(...args);
    };
  }
};

export default SimpleCanPlugin;
