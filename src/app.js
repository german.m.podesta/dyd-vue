import Vue from 'vue'

import 'normalize.css/normalize.css'// A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n
import VueI18n from 'vue-i18n'
import VueLuxon from 'vue-luxon'
import SimpleCanPlugin from './can'

import '@/styles/index.scss' // global css

import App from './App'
import router from './router'
import store from './store'
import registerFilters from './filters'

import '@/icons' // icon registration
import '@/feathers'

// TODO: Use vue-meta or something similar to handle the title
document.title = 'D&D Panificados'

Vue.use(ElementUI, {
  locale
})
Vue.use(VueI18n)
Vue.use(VueLuxon, {
  serverZone: 'utc',
  serverFormat: 'iso',
  clientZone: 'America/Buenos_Aires',
  clientFormat: 'locale'
})
Vue.use(SimpleCanPlugin)

const messages = {
  es: {
    'salesman': 'Vendedor',
    'administrator': 'Administrador',
    'User': 'Usuario',
    'Product': 'Producto',
    'Order': 'Pedido',
    'Orders': 'Pedidos',
    'draft': 'Borrador',
    'presented': 'Presentado',
    'confirmed': 'Confirmado',
    'delivered': 'Entregado'
  }
}

const i18n = new VueI18n({
  locale: 'es', // set locale
  messages // set locale messages
})

Vue.config.productionTip = false
registerFilters()

new Vue({
  el: '#app',
  i18n,
  router,
  store,
  template: '<App/>',
  components: { App }
})
