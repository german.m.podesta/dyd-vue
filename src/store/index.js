import Vue from "vue";
import Vuex from "vuex";
import app from "./modules/app";
import user from "./modules/user";
import permission from "./modules/permission";
import getters from "./getters";
import feathersVuex from "feathers-vuex";
import feathersClient from "../feathers";
const { service, auth, FeathersVuex } = feathersVuex(feathersClient, {
  idField: "_id"
});

Vue.use(Vuex);
Vue.use(FeathersVuex);

const store = new Vuex.Store({
  modules: {
    app,
    user,
    permission
  },
  getters,
  plugins: [
    service("clients"),
    service("products"),
    auth({ userService: "users" })
  ]
});

export default store;
