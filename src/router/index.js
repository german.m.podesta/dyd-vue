import Vue from 'vue'
import Router from 'vue-router'
import auth from '@/services/auth'
import salesmenRoutes from './salesmen'
import productsRoutes from './products'
import usersRoutes from './users'
import myOrders from './my-orders'
import ordersRoutes from './orders'
import clientsRoutes from './clients'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

// const Dashboard = () => import('@/views/dashboard/index')
const Login = () => import('@/views/login')
const error404 = () => import('@/views/404')
const OrdersPrint = () => import('@/views/orders/print')

function redirectNotLogged(to, from, next) {
  // NOTE: This sets the logged user on refresh
  return auth.authenticate()
    .then(() => {next(); return null})
    .catch(() => {next({name: 'login'}); return null}) // TODO: Send error message

}

const routes = [{
  path: '/', // Go to the dashboard or to the login page
  beforeEnter(to, from, next) {
    // Also return null in the promise?
    return auth.authenticate()
      .then(user => {next({ name: 'dashboard-home' })})
      .catch(() => {next('/login')})
  }
}, { // TODO: Move into orders
  path: '/dashboard/reports/:id', name: 'orders-print',
  component: OrdersPrint, props: true, beforeEnter: redirectNotLogged
  // TODO: Check authentication
}, {
  path: '/login', name: 'login', component: Login
}, {
  path: '/404', name: 'error', component: error404
}, {
  name: 'dashboard',
  path: '/dashboard',
  redirect: '/dashboard/home',
  beforeEnter: redirectNotLogged,
  component: Layout,
  children: [
    {
      name: 'dashboard-home',
      path: 'home',
      beforeEnter(to, from, next) {
        const user = auth.getUser()
        if (user.role === 'administrator') {
          next({ name: 'orders-list' })
        } else {
          next({ name: 'my-orders-list' })
        }
        return null
      }
    },
    productsRoutes,
    salesmenRoutes,
    myOrders,
    ordersRoutes,
    usersRoutes,
    clientsRoutes
  ]
}, {
  path: '*', redirect: '/404'
}]

const router = new Router({
  // mode: 'history', //后端支持可开
  mode: 'hash',
  scrollBehavior: () => ({ y: 0 }),
  routes
})

export default router
