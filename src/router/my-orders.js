import Layout from '../views/layout/Layout'
import ParentLayout from '../views/layout/components/parent-layout'

const MyOrdersList = () => import('@/views/my-orders/list')
const MyOrdersCreate = () => import('@/views/my-orders/create')
const MyOrdersUpdate = () => import('@/views/my-orders/update')

export default {
  component: ParentLayout,
  path: 'my-orders',
  redirect: 'my-orders/list',
  name: 'my-orders',
  meta: {
    title: 'Mis Pedidos', icon: 'shopping-cart',
    can: ['find', 'MyOrders']
  },
  children: [{
    path: '',
    name: 'my-orders-list',
    component: MyOrdersList,
    meta: {
      title: 'Listado', icon: 'clipboard-list',
      can: ['find', 'MyOrders']
    }
  }, {
    path: 'create',
    name: 'my-orders-create',
    component: MyOrdersCreate,
    meta: {
      title: 'Nuevo Pedido', icon: 'cart-plus',
      can: ['create', 'MyOrders']
    }
  }, {
    path: 'update/:id',
    name: 'my-orders-update',
    component: MyOrdersUpdate,
    props: true,
    meta: { can: ['update', 'MyOrders']}
  }]
}
