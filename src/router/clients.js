import ParentLayout from "../views/layout/components/parent-layout";

const ClientsList = () => import("@/views/clients/list");
const ClientsCreate = () => import("@/views/clients/create");
const ClientsUpdate = () => import("@/views/clients/update");

export default {
  component: ParentLayout,
  path: "clients",
  redirect: "clients/list",
  name: "clients",
  meta: {
    title: "Clientes",
    icon: "handshake",
    can: ["find", "Clients"]
  },
  children: [
    {
      path: "",
      name: "clients-list",
      component: ClientsList,
      meta: {
        title: "Listado",
        icon: "clipboard-list",
        can: ["find", "Clients"]
      }
    },
    {
      path: "create",
      name: "clients-create",
      component: ClientsCreate,
      meta: {
        title: "Nuevo Cliente",
        icon: "cart-plus",
        can: ["create", "Clients"]
      }
    },
    {
      path: "update/:id",
      name: "clients-update",
      component: ClientsUpdate,
      props: true,
      meta: { can: ["update", "Clients"] }
    }
  ]
};
