import Layout from '../views/layout/Layout'
import ParentLayout from '../views/layout/components/parent-layout'

const SalesMenList = () => import('@/views/salesmen/list')
const SalesMenNew = () => import('@/views/salesmen/new')
const SalesMenUpdate = () => import('@/views/salesmen/update')

export default {
  component: ParentLayout,
  path: 'salesmen',
  redirect: 'salesmen/list',
  name: 'salesmen',
  meta: {
    title: 'Vendedores', icon: 'users',
    can: ['find', 'SalesMen']
  },
  children: [{
    path: '',
    name: 'salesmen-list',
    component: SalesMenList,
    meta: {
      title: 'Listado', icon: 'list',
      can: ['find', 'SalesMen']
    }
  }, {
    path: 'new',
    name: 'salesmen-new',
    component: SalesMenNew,
    meta: {
      title: 'Nuevo', icon: 'plus',
      can: ['create', 'SalesMen']
    }
  }, {
    path: 'update/:id',
    name: 'salesmen-update',
    component: SalesMenUpdate,
    props: true,
    meta: {
      can: ['update', 'SalesMen'],
    }
  }]
}
