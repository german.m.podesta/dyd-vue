import Layout from '../views/layout/Layout'
import ParentLayout from '../views/layout/components/parent-layout'

const UsersList = () => import('@/views/users/list')
const UsersCreate = () => import('@/views/users/create')
const UsersUpdate = () => import('@/views/users/update')

export default {
  component: ParentLayout,
  path: 'users',
  redirect: '/users/list',
  name: 'users',
  meta: {
    title: 'Usuarios', icon: 'key',
    can: ['find', 'Users']
  },
  children: [{
    path: '',
    name: 'users-list',
    component: UsersList,
    meta: {
      title: 'Listado', icon: 'list',
      can: ['find', 'Users']
    }
  }, {
    path: 'create',
    name: 'users-create',
    component: UsersCreate,
    meta: {
      title: 'Nuevo Usuario', icon: 'plus',
      can: ['create', 'Users']
    }
  }, {
    path: 'update/:id',
    name: 'users-update',
    component: UsersUpdate,
    props: true,
    meta: { can: ['update', 'Users']}
  }]
}
