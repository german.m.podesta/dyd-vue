import Layout from '../views/layout/Layout'
import ParentLayout from '../views/layout/components/parent-layout'

const ProductsList = () => import('@/views/products/list')
const ProductsCreate = () => import('@/views/products/create')
const ProductsUpdate = () => import('@/views/products/update')

export default {
  component: ParentLayout,
  path: 'products',
  redirect: 'products/list',
  name: 'products',
  meta: {
    title: 'Productos', icon: 'cubes',
    can: ['find', 'Products']
  },
  children: [{
    path: '',
    name: 'products-list',
    component: ProductsList,
    meta: {
      title: 'Listado', icon: 'list',
      can: ['find', 'Products']
    }
  }, {
    path: 'create',
    name: 'products-create',
    component: ProductsCreate,
    meta: {
      title: 'Nuevo Producto', icon: 'plus',
      can: ['create', 'Products']
    }
  }, {
    path: 'update/:id',
    name: 'products-update',
    component: ProductsUpdate,
    props: true,
    meta: { can: ['update', 'Products']}
  }]
}
