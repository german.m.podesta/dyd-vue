import ParentLayout from '../views/layout/components/parent-layout'

const OrdersList = () => import('@/views/orders/list')
const OrdersCreate = () => import('@/views/orders/create')
const OrdersUpdate = () => import('@/views/orders/update')
const OrdersExport = () => import('@/views/orders/export')

export default {
  component: ParentLayout,
  path: 'orders',
  redirect: 'orders/list',
  name: 'orders',
  meta: {
    title: 'Pedidos', icon: 'shopping-cart',
    can: ['find', 'Orders']
  },
  children: [{
    path: '',
    name: 'orders-list',
    component: OrdersList,
    meta: {
      title: 'Listado', icon: 'clipboard-list',
      can: ['find', 'Orders']
    }
  }, {
    path: 'create',
    name: 'orders-create',
    component: OrdersCreate,
    meta: {
      title: 'Nuevo Pedido', icon: 'cart-plus',
      can: ['create', 'Orders']
    }
  }, {
    path: 'export',
    name: 'orders-export',
    component: OrdersExport,
    meta: {
      title: 'Sincronizar Excel', icon: 'file-excel',
      can: ['export', 'Orders']
    }
  }, {
    path: 'update/:id',
    name: 'orders-update',
    component: OrdersUpdate,
    props: true,
    meta: { can: ['update', 'Orders']}
  }]
}
