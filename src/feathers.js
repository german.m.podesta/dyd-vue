import feathers from "@feathersjs/feathers";
import socketio from "@feathersjs/socketio-client";
import auth from "@feathersjs/authentication-client";
import io from "socket.io-client";
import localforage from "localforage";

const BACKEND_URL = process.env.BACKEND_ENDPOINT;

const socket = io(BACKEND_URL, { transports: ["websocket"] });

const api = feathers()
  .configure(socketio(socket))
  .configure(auth({ storage: localforage }));

export default api;
