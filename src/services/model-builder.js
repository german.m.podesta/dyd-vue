/*
 * This is kind of hack to make casl work with model names
 */

const models = {
  MyOrders: function() {},
  Orders: function() {},
  Products: function() {},
  Clients: function() {},
  Salesmen: function() {},
  Users: function() {}
}

class ModelBuilder {
  constructor(modelName) {
    if (!(modelName in models)) {
      throw new Error(`modelName '${modelName}' not found`)
    }
    this.modelName = modelName
  }

  build(rawModel) {
    rawModel.constructor = models[this.modelName]
    return rawModel
  }
}

export { ModelBuilder }
