import api from '@/feathers'
import { Ability } from '@casl/ability'

const RULES = {
  'salesman': [{
    'actions': ['find'],
    'subject': 'Products'
  }, {
    'actions': [],
    'subject': 'Orders'
  }, {
    'actions': [],
    'subject': 'SalesMen'
  }, {
    'actions': ['find', 'create'],
    'subject': 'MyOrders'
  }, {
    'actions': ['update', 'remove'],
    'subject': 'MyOrders',
    'conditions': { state: 'draft' }
  }, {
    'actions': ['find', 'create'],
    'subject': 'Clients'
  }, {
    'actions': ['find'],
    'subject': 'Misc'
  }],
  'administrator': [{
    'actions': ['find', 'create', 'update', 'remove'],
    'subject': 'Products'
  }, {
    'actions': ['find', 'create', 'update', 'remove', 'print', 'export'],
    'subject': 'Orders'
  }, {
    'actions': ['find', 'create', 'update', 'remove'],
    'subject': 'SalesMen'
  }, {
    'actions': ['find', 'create', 'update', 'remove'],
    'subject': 'Users'
  }, {
    'actions': ['find', 'create', 'update', 'remove', 'print'],
    'subject': 'Orders'
  }, {
    'actions': ['find', 'create', 'update', 'remove'],
    'subject': 'MyOrders'
  }, {
    'actions': ['find', 'create', 'update', 'remove'],
    'subject': 'Clients'
  }, {
    'actions': ['find'],
    'subject': 'Misc'
  }]
}

const auth = {

  user: null,
  abilities: null,

  can(...rule) {
    const abilities = this._getAbilities()
    return abilities.can(...rule)
  },

  getUser() {
    return this.user
  },

  getRoles() {
    return Object.keys(RULES)
  },

  _getAbilities() {
    if ( this.abilities ) {
      return this.abilities
    }

    this.abilities = new Ability()
    this.abilities.update(RULES[this.user.role])
    return this.abilities
  },

  fetchUser (accessToken) {
    return api.passport.verifyJWT(accessToken)
      .then(payload => {
        return api.service('users').get(payload.userId)
      })
      .then(user => {
        return Promise.resolve(user)
      })
  },

  authenticate () {
    return api.authenticate()
      .then((response) => {
        return this.fetchUser(response.accessToken)
      })
      .then(user => {
        this.user = user
        return Promise.resolve(user)
      })
      .catch((err) => {
        this.user = null
        return Promise.reject(err)
      })
  },

  authenticated () {
    return this.user != null
  },

  signout () {
    return api.logout()
      .then(() => {
        this.user = null
        this.abilities = null
      })
      .catch((err) => {
        return Promise.reject(err)
      })
  },

  onLogout (callback) {
    api.on('logout', () => {
      this.user = null
      this.abilities = null
      callback()
    })
  },

  onAuthenticated (callback) {
    api.on('authenticated', response => {
      this.fetchUser(response.accessToken)
      .then(user => {
        this.user = user
        callback(this.user)
      })
      .catch(err => {
        callback(this.user)
      })
    })
  },

  register (email, password) {
    return api.service('users').create({
      email: email,
      password: password
    })
  },

  login ({email, password}) {
    return api.authenticate({
      strategy: 'local',
      email: email,
      password: password
    })
  }

}

export default auth
