const orderStates = {
  getStates() {
    return [
      'draft',
      'presented',
      'confirmed',
      'delivered'
    ]
  },
  getSalesmenStates() {
    return [
      'draft',
      'presented'
    ]
  },

  tagMap: {
    'draft': 'info',
    'presented': 'warning',
    'confirmed': 'primary',
    'delivered': 'success'
  },
  getColor(state) {
    return this.tagMap[state]
  },
  getDefault() {
    return 'draft'
  },
  getDescription() {
    return [
      {
        name: 'Borrador',
        description: 'Versión preliminar del pedido, puede ser editada por el vendedor'
      },
      {
        name: 'Presentado',
        description: 'El pedido ya fue confirmado por el vendedor, pasada esta etapa solo puede ser editado por el administrador'
      },
      {
        name: 'Confirmado',
        description: 'El pedido fue confirmado por el jefe de ventas, solo resta enviarlo'
      },
      {
        name: 'Enviado',
        description: 'El pedido ya fué enviado al cliente'
      }
    ]
  }
}

export default orderStates
