class Formatter {
  constructor () {
    this.currencyFormatter = new Intl.NumberFormat('es-AR', {
      style: 'currency',
      currency: 'ARS',
      minimumFractionDigits: 2
    })
  }
  formatCurrency (value) {
    return this.currencyFormatter.format(value)
  }
}

export default new Formatter()
