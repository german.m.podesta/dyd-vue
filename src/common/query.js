import {get, pickBy} from 'lodash'

function filterQuery (baseQuery, filters) {
  const query = Object.assign({}, baseQuery, filters)
  return pickBy(query, (searchTerm) => {
    if (searchTerm === '') {
      return false
    } else if (searchTerm === null) {
      return false
    } else if (get(searchTerm, '$in')) {
      return searchTerm.$in.length > 0
    } else {
      return true
    }
  })
}

// TODO: Continue this implementation
class FilterQuery {
  constructor (baseQuery) {
    this.baseQuery = baseQuery
    this.pickingRules = [
      this.excludeEmptyFields
    ]
  }

  _excludeEmptyFields (searchTerm) {
    return (searchTerm === '' || searchTerm === null)
  }

  inclusionRule (newRule) {
    this.pickingRules.push(newRule)
  }

  filterQuery (filters) {
    const query = Object.assign({}, filters)
    return pickBy(query, this._excludeEmptyFields)
  }
}

export {FilterQuery, filterQuery}
