import errors from "@feathersjs/errors";

import { ValidationError } from "@/exceptions";

export default {
  data() {
    return {
      errors: {
        // Complete this
      },
      fields: {
        // Complete this
      },
      nonFieldErrors: [], // TODO: Check that
      rules: {}
    };
  },
  computed: {
    nonFieldError() {
      return this.nonFieldErrors[0] || null;
    }
  },
  methods: {
    buildForm(rules) {
      let fields = {};
      let errors = {};
      for (let ruleName in rules) {
        fields[ruleName] = "";
        errors[ruleName] = "";
      }
      return { fields, errors };
    },
    clear() {
      for (let key in this.fields) {
        this.fields[key] = "";
      }
      this.$refs.fields.resetFields();
    },
    async asyncValidate() {
      try {
        return this.$refs.fields.validate();
      } catch (e) {
        return false;
      }
    },
    update() {
      return this.service.update(this.id, this.fields).then(updatedSalesman => {
        this.$message({
          type: "success",
          message: "Datos guardados correctamente"
        });
      });
    },
    create() {
      return this.service.create(this.fields).then(createdSalesman => {
        this.$message({
          type: "success",
          message: "Datos guardados correctamente"
        });
        this.clear();
      });
    },
    submit() {
      return this.validate()
        .then(() => this[this.action]())
        .caught(ValidationError, error => {
          this.$message({
            type: "warning",
            message:
              "Falló la validación, por favor verifique los mensajes de error."
          });
        })
        .caught(errors.BadRequest, errorResponse => {
          this.$message({
            type: "warning",
            message: "Error al confirmar los datos"
          });
          this.populateBackendErrors(
            errorResponse.errors,
            (fieldName, fieldContent) => fieldContent.message
          );
        })
        .caught(errors.Conflict, errorResponse => {
          this.populateBackendErrors(
            errorResponse.errors,
            (conflictingField, conflictingValue) =>
              `Ya existe un registro con ${conflictingField} "${conflictingValue}"`
          );
        });
    },
    validate() {
      return this.$refs.fields.validate().catch(() => {
        throw new ValidationError(
          "Falló la validación, por favor verifique los mensajes de error."
        );
      });
    },
    populateBackendErrors(errors, getMessage) {
      let nonFieldErrors = [];
      for (let fieldName in errors) {
        if (fieldName in this.errors) {
          this.errors[fieldName] = getMessage(fieldName, errors[fieldName]);
        } else {
          nonFieldErrors.push(getMessage(fieldName, errors[fieldName]));
        }
      }
      for (let errorMessage of nonFieldErrors) {
        this.$message({
          type: "warning",
          message: errorMessage
        });
      }
    }
  }
};
