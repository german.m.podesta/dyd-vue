
import { debounce } from 'lodash'

export default {
  data () {
    return {
      collection: [],
      listLoading: true
    }
  },
  created () {
    this.filter = debounce(this.filter, 500)
  },
  computed: {
    canCreate() {
      return this.$can('create', this.resourceName.plural)
    },
    canRemove() {
      return this.$can('remove', this.resourceName.plural)
    },
    canUpdate() {
      return this.$can('update', this.resourceName.plural)
    },
    canPrint() {
      return this.$can('print', this.resourceName.plural)
    }
  },
  methods: {
    go(subRoute, id=null, newTab=false) {
      const routeName = `${this.routePrefix}-${subRoute}`
      const routeDescr = {name: routeName}

      if (id) {
        routeDescr['params'] = {id}
      }

      if (newTab) {
        const routeData = this.$router.resolve(routeDescr)
        window.open(routeData.href, '_blank')
      } else {
        this.$router.push(routeDescr)
      }
    },
    async filter() {
      this.listLoading = true
      this.rows = await this.service.find({
        query: this.query
      })
      this.listLoading = false
    },
    paginate(pageNumber) {
      this.query.$skip = (pageNumber - 1) * this.query.$limit
      this.fetchCollection()
    },
    async fetchCollection() {
      this.loading = true
      const response = await this.service.find({
        query: this.query
      })
      this.collection = response
      this.loading = false
    },
    remove(row) {
      return this
        .$confirm('¿Está seguro que desea eliminar el registro?')
        .then(() =>  this.service.remove(row._id))
      // TODO: Hacer que el refresh lo maneje con el evento de socketio
        .then(() => this.fetchCollection())
    }
  }
}
