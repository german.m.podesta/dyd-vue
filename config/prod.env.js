'use strict'
module.exports = {
  NODE_ENV: '"production"',
  BACKEND_ENDPOINT: process.env.BACKEND_ENDPOINT || '"https://api.staging.dyd.space"'
}
